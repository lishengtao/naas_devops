#-*-coding:utf-8-*-
# Linda Tian  May 2020
# linda.tian@telus.com
# Get VCE information from all enterprise in an given VCO
# Usage:python velo_get_vce_p3.py  -u <USERNAME> -vco <VCO_HOSTNAME>

from client import *
import argparse
from getpass import getpass
import time
import csv
import HTML_p3 as HTML
import tabulate
import click

def get_args():
    parser = argparse.ArgumentParser(
    description='Velo cloud script to GET all edges on VCO network elements!')
    parser.add_argument('-u', '--vco_user', metavar='vco_username', help='username to connect to VCO [REQUIRED]', required=True)
    parser.add_argument('-vco', '--vco_name', metavar='vco_pname', help='VCO name to connect to Environment [REQUIRED]',
                        required=True)
    return parser

def main():

    global USERNAME
    global VCO_HOSTNAME
    global PASSWORD

    args_parser = get_args()
    args = args_parser.parse_args()

    VCO_HOSTNAME = None
    USERNAME = None
    PASSWORD = None

    if args.vco_user is not None:
        USERNAME = args.vco_user
    if args.vco_name is not None:
        VCO_HOSTNAME = args.vco_name

    PASSWORD = getpass(prompt='Please input VSO password: ')

    client = VcoRequestManager(VCO_HOSTNAME)
    client.authenticate(USERNAME, PASSWORD, is_operator=False)

    ent = client.call_api('enterpriseProxy/getEnterpriseProxyEnterprises', {})
    enterprise_dict = {}
    enterprise_dict_values = ''
    enterprise_dict_keys = ''
    edge_dict = {}
    edge_dict_values = ''
    edge_dict_keys = ''
    EDGE_list = []

    print ('ALL VCE in the VCO:  ', VCO_HOSTNAME)
    for ent_item in ent:
        for keys, values in ent_item.items():
            if keys == "name":
                enterprise_dict_keys = values
            if keys == "id":
                enterprise_dict_values = values

        enterprise_dict[enterprise_dict_keys] = enterprise_dict_values

        ENTERPRISE_NAME = enterprise_dict_keys
        ENTERPRISE_ID = enterprise_dict_values
        print ('Collecting VCE in enterprise: ', ENTERPRISE_NAME)
        edges = client.call_api('enterprise/getEnterpriseEdges', {
            'enterpriseId': ENTERPRISE_ID
        })
        for edge in edges:
            EDGE_ID = edge['id']
            edge_stack_json_body = {
                "enterpriseId": ENTERPRISE_ID,
                "edgeId": EDGE_ID
            }
            edge_stack_response = client.call_api('edge/getEdgeConfigurationStack', edge_stack_json_body)
            ip_address = ''
            interface = ''
            for config in edge_stack_response:
                if config['name'] == 'Edge Specific Profile':
                    for config_module in config['modules']:
                        if config_module['name'] == "WAN":
                            edge_specific_wan_data = config_module['data']
                            if 'networks' in edge_specific_wan_data:
                                for uplink in edge_specific_wan_data['networks']:
                                    ip_address = uplink['ipAddress']
                                    interface  = uplink['interface']
                                    wan_SP_name = uplink['name']
                                    EDGE_list.append([ENTERPRISE_NAME, edge['name'],ip_address, interface, wan_SP_name,
                                                      edge['edgeState'], edge['softwareVersion'],
                                                      edge['deviceFamily'],
                                                      edge['serviceState'], edge['isHub'],
                                                      edge['selfMacAddress'], edge['serialNumber'],
                                                      edge['created'], edge['activationTime']])
                            else:
                                EDGE_list.append([ENTERPRISE_NAME, edge['name'], ip_address, interface, wan_SP_name,
                                                  edge['edgeState'], edge['softwareVersion'],
                                                  edge['deviceFamily'],
                                                  edge['serviceState'], edge['isHub'],
                                                  edge['selfMacAddress'], edge['serialNumber'],
                                                  edge['created'], edge['activationTime']])

    # print to screen with table format
    header = ['Enterprise', 'Edge_name',  'UPlink_IP', 'Interface', 'SP_NAME',
              'edgeState', 'softwareVersion',
              'deviceFamily', 'serviceState', 'isHub','selfMacAddress', 'serialNumber',
              'Creation_Time','activation_Time']

    try:
        click.echo(tabulate.tabulate(EDGE_list, header, tablefmt="orgtbl"))
        print(tabulate.tabulate(EDGE_list, header, tablefmt="orgtbl"))
    except UnicodeEncodeError:
        click.echo(tabulate.tabulate(EDGE_list, header, tablefmt="grid"))
        click.echo(table)
        print(tabulate.tabulate(EDGE_list, header, tablefmt="grid"))

    # Save result to csv file
    time_str = time.strftime("%Y%m%d-%H%M%S")

    filename = 'VCE_list_%s.csv' % time_str
    with open(filename, 'w', ) as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',',
                                quotechar=',', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(header)
        for i in EDGE_list:
            spamwriter.writerow(i)

    print('##########################################################################################################')
    print('Saving to HTML FILES')
    # EDGE_list.insert(0, header)
    HTMLFILE = 'VCE.html'
    with open(HTMLFILE, 'w+') as f:
        table_data = EDGE_list
        VCE_htmlcode = HTML.table(table_data, header_row=header, col_styles=['background-color:#f2e6ff','background-color:#f2e6ff',
                                                                             'background-color:#f2e6ff','background-color:#f2e6ff',
                                                                             'background-color:#f2e6ff','background-color:#f2e6ff',
                                                                             'background-color:#f2e6ff','background-color:#f2e6ff',
                                                                             'background-color:#f2e6ff','background-color:#f2e6ff',
                                                                             'background-color:#f2e6ff','background-color:#f2e6ff',
                                                                             'background-color:#f2e6ff','background-color:#f2e6ff'])

        f.write(VCE_htmlcode)
    print ('Task Completed!')

if __name__ == '__main__':
    main()
